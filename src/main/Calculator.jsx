import React from 'react'
import './Calculator.css'
import Button from '../components/Button'
import Display from '../components/Display'

const initialState = {
  displayValue: '0',
  shouldClearDisplay: false,
  operation: null,
  values: [0, 0],
  currentIndex: 0,
}

export default class Calculator extends React.Component {
  state = { ...initialState }

  clearMemory() {
    this.setState({ ...initialState })
  }

  setOperation(mathOperator) {
    this.setState({ displayValue: mathOperator }) // Shows on the display the chosen math operator (/, *, -, +)

    if (this.state.currentIndex === 0) { // If is using the first index...
      this.setState({ 
        operation: mathOperator,
        currentIndex: 1,
        shouldClearDisplay: true })
    } else { // When the second is being used...
      const isEqualsOperation = mathOperator === '='
      const currentOperation = this.state.operation // In case the user uses the operator more than one time
      const values = [...this.state.values]

      try {
        values[0] = eval(`${values[0]} ${currentOperation} ${values[1]}`) // do the math
      } catch (e) {
        values[0] = this.state.values[0]
        console.error(`Invalid operation`)
      }

      values[1] = 0 // Reset the second index of values

      this.setState({ // Applies the result to our state
        displayValue: values[0],
        operation: isEqualsOperation ? null : mathOperator,
        currentIndex: isEqualsOperation ? 0 : 1,
        shouldClearDisplay: !isEqualsOperation
      })
    }
  }

  addDigit(digit) {
    // should allow only one dot digit
    if (digit === '.' && this.state.displayValue.includes('.')) {
      return
    }

    const shouldClearDisplay = this.state.displayValue === '0' || this.state.shouldClearDisplay // conditions to clear display
    const currentValue = shouldClearDisplay ? '' : this.state.displayValue // in case it should clear the display
    const newDisplayValue = currentValue + digit // get display typed value

    // refreshs the state with the entered values
    this.setState({
      displayValue: newDisplayValue,
      shouldClearDisplay: false
    })

    if (digit !== '.') {
      const valuesActiveIndex = this.state.currentIndex // gets the current index
      const newValue = parseFloat(newDisplayValue) // gets the new typed digit
      const values = [...this.state.values] // clones the values from the state

      values[valuesActiveIndex] = newValue

      this.setState({ values }) // updates the values
    }
  }

  render() {
    const addDigit = digit => this.addDigit(digit)
    const setOperation = op => this.setOperation(op)

    return (
      <div className="calculator">
        <Display value={this.state.displayValue} />
        <Button triple label='AC' click={() => this.clearMemory()} />
        <Button label='/' click={setOperation} operation />
        <Button label='7' click={addDigit} />
        <Button label='8' click={addDigit} />
        <Button label='9' click={addDigit} />
        <Button label='*' click={setOperation} operation />
        <Button label='4' click={addDigit} />
        <Button label='5' click={addDigit} />
        <Button label='6' click={addDigit} />
        <Button label='+' click={setOperation} operation />
        <Button label='1' click={addDigit} />
        <Button label='2' click={addDigit} />
        <Button label='3' click={addDigit} />
        <Button label='-' click={setOperation} operation />
        <Button label='0' click={addDigit} double />
        <Button label='.' click={addDigit} />
        <Button label='=' click={setOperation} operation />
      </div>
    )
  }
}